#!/usr/bin/python

import difflib
import pandas as pd
import sys
import os
from zipfile import ZipFile
from PyQt5.QtWidgets import QApplication, QPushButton, QWidget, QFileDialog
from pathlib import Path
from pyairtable import Table, Api, retry_strategy
from pyairtable.formulas import match

api = Api(os.environ['AIRTABLE_API_KEY'], retry_strategy=
          retry_strategy(total=10))
AT_key = os.environ['AIRTABLE_API_KEY']
table = api.table('appZtsBCSTUnzv9Ui', 'Members')


class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'Unzip memberlist'
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.openFileNameDialog()
        # self.openFileNamesDialog()
        # self.saveFileDialog()
        qbtn = QPushButton('Quit', self)
        qbtn.clicked.connect(QApplication.instance().quit)
        qbtn.resize(qbtn.sizeHint())
        qbtn.move(50, 50)

        self.show()

    def getFileName(self):
        return self.fileName

    def getCSVFile(self):
        return self.csvFile

    def openFileNameDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        self.fileName, _ = QFileDialog.getOpenFileName(
            self, "QFileDialog.getOpenFileName()", "", "Zip Files (*.zip);;\
            All Files (*)", options=options)
        self.csvFile = ''.join([self.fileName.split('.')[0], '.csv'])
        if self.fileName:
            print("self.fileName: {}".format(self.fileName))
            print("self.csvFile: {}".format(self.csvFile))
        with ZipFile(self.fileName, 'r') as zipObj:
            # print(Path(fileName).parent)
            zipObj.extractall(Path(self.fileName).parent)

    def saveFileDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(
            self, "QFileDialog.getSaveFileName()", "", "CSV Files (*.csv)",
            options=options)
        if fileName:
            print(fileName)
        print(self.openFile)

    def checkNewMembers(self):
        pass


def f(x):
    try:
        return difflib.get_close_matches(x["city"], place_names['NAME'])[0]
    except IndexError:
        return ''
    except TypeError:
        return ''


def g(x):
    try:
        return list(place_names['COUNTY'].loc
                    [place_names['NAME'] == x["city"]])[0]
    except IndexError:
        return ''


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()

    place_names = pd.read_excel('Place_names.ods')

    print("{}".format(Path(ex.getFileName())))
    memberlist = pd.read_csv(Path(ex.getCSVFile()), dtype={'best_phone':
                                                           'string'})
    # Read in the membership list and the
    memberlist.apply(lambda x: x.astype(str).str.title())

    # fix cities and add county
    memberlist['city'] = memberlist.apply(f, axis=1)
    memberlist['county'] = memberlist.apply(g, axis=1)

    fix_subset = ['actionkit_id', 'first_name', 'last_name',
                  'address1', 'city', 'zip', 'best_phone', 'email',
                  'do_not_call', 'p2ptext_optout', 'join_date', 'xdate',
                  'membership_type', 'membership_status',
                  'union_member', 'union_name', 'union_local', 'county']

    out_file = '_'.join([ex.getCSVFile().split('.')[0], '_fixed.csv'])
    member_sub_file = '_'.join([ex.getCSVFile().split('.')[0], '_subset.csv'])
    print("out_file: {}".format(out_file))
    memberlist[fix_subset].to_csv(out_file, index=False)
    print("member_sub_file:{}".format(member_sub_file))
    # save a subset as csv. Easier to work with
    member_subset = ['actionkit_id', 'first_name', 'last_name', 'city',
                     'city', 'best_phone', 'email', 'do_not_call',
                     'p2ptext_optout', 'join_date', 'xdate', 'membership_type',
                     'membership_status', 'county']
    memberlist[member_subset].to_csv(member_sub_file, index=False)

    # upload to airtable
    memberlist = memberlist.fillna('')
    memberlist.astype({'best_phone': 'string'}).dtypes
    member_dict = memberlist[fix_subset].to_dict('records')
    print("not in airtable:")
    added_members = 0
    deleted_members = 0
    # add actionkit_ids not in airtable
    for curr_member in member_dict:
        # print(type(member_id))
        member_id = curr_member['actionkit_id']
        formula = match({"actionkit_id": member_id})
        record = table.first(formula=formula)
        if curr_member['do_not_call'] == '':
            curr_member['do_not_call'] = False
        else:
            curr_member['do_not_call'] = True
        if curr_member['p2ptext_optout'] == '':
            curr_member['p2ptext_optout'] = False
        else:
            curr_member['p2ptext_optout'] = True

        if record is None:
            added_members += 1
            curr_member['Contact status'] = 'Needs Welcome'
            print("inserting: {}".format(member_id))
            table.create(curr_member, typecast=True)
        # check changes
        else:
            table.update(record['id'], fields=curr_member, replace=True,
                         typecast=True)
    # Loop over airtable removing anyone not in current memberlist
    # Remove people who fall off the memberlist
    airtable_all = table.all(view="Grid view", fields=["actionkit_id"])
    try:
        for record in airtable_all:
            curr_id = record["fields"]["actionkit_id"]
            # print(type(curr_id))
            # print(type(memberlist["actionkit_id"]))
            members = memberlist["actionkit_id"].to_list()
            if curr_id not in members:
                print("{} not in memberlist".format(curr_id))
                table.delete(record['id'])
                deleted_members += 1
    except KeyError as k:
        print(record)
        print("Variable {} is not defined".format(k))
    print("Added: {} members".format(added_members))
    print("Deleted: {} members".format(deleted_members))
    sys.exit(app.exec_())
