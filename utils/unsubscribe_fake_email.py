#!/usr/bin/python
from pyairtable import Table
# from pyairtable.formulas import match
import os
import requests
import json

AT_api_key = os.environ["AIRTABLE_API_KEY"]
table = Table(AT_api_key, 'appamF45TypezsKPY', 'Expired Members')
AN_api_key = os.environ["AN_API_KEY"]
headers = {'OSDI-API-Token': AN_api_key, 'Content-Type': 'application/json'}
# print(r)
more_pages = True
base_url = 'https://actionnetwork.org/api/v2/people'
url = base_url
while (more_pages):
    r = requests.get(url, headers=headers).json()
    for person in r['_embedded']['osdi:people']:
        custom_fields = person['custom_fields']
        if 'AK_ID' not in custom_fields:
            continue
        ak_id = person['custom_fields']['AK_ID']
        sub_status = person["email_addresses"][0]["status"]
        try:
            xdate = person['custom_fields']['Xdate']
            given_name = person["given_name"]
            if (sub_status == 'subscribed'):
                if 'example.com' in person["email_addresses"][0]["address"]:
                    person_id = person['identifiers'][0]
                    person_url = '/'.join([base_url, person_id.split(':')[1]])
                    print('!!!{}!!!\n'.format(person_url))
                    # post to unsubscribe
                    # data = {"email_addresses": [{"status": "unsubscribed"}]}
                    payload = {
                        'email_addresses': [{
                            'status': 'unsubscribed'
                        }]
                    }
                    print("json: {}".format(json.dumps(payload)))
                    resp = requests.put(person_url, json=payload,
                                        headers=headers)
                    print(resp.text)
        except KeyError:
            print("key error")
    try:
        url = r["_links"]["next"]["href"]
    except KeyError:
        print(url)
        print("No more pages")
        more_pages = False
