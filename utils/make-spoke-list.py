#!/usr/bin/python

import pandas as pd
import sys
from os import path

memberlist = pd.read_csv(sys.argv[1], dtype='object')
filename = path.splitext(sys.argv[2])[0]
print(filename)
out_filename = ''.join([filename, '-spoke.csv']) if len(sys.argv) >= 3\
    else 'mhvdsa-spokelist.csv'

''
spokelist = memberlist[['first_name', 'last_name', 'best_phone']]
print(spokelist.dtypes)
# spokelist.astype({'best_phone': 'object'}).dtypes

# .where(
#                           memberlist['Mobile_Phone'].isnull(),
#                           memberlist['Mobile_Phone'])], axis=1)

spokelist.rename(columns={'best_phone': 'cell'}, inplace=True)
spokelist.rename(columns={'first_name': 'firstName'}, inplace=True)
spokelist.rename(columns={'last_name': 'lastName'}, inplace=True)
spokelist['firstName'] = spokelist['firstName'].str.title()

spokelist.to_csv(out_filename)
